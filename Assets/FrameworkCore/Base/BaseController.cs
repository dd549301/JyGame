﻿/*
 *  date: 2018-04-21
 *  author: John-chen
 *  cn: 游戏基础控制器 todo: 暂时没用
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 游戏基础控制器
    /// </summary>
    public class BaseController
    {
        public virtual void Init()
        {

        }
        public virtual void Remove()
        {

        }
    }
}
