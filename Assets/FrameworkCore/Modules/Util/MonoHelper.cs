﻿/*
 *  date: 2018-07-12
 *  author: John-chen
 *  cn: Mono帮助类
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// Mono帮助类
    /// </summary>
    public class MonoHelper : MonoBehaviour
    {
        public static MonoHelper Ins { get { return _ins; } }
        private static MonoHelper _ins;

        private void Awake()
        {
            
        }

        private void Start()
        {

        }

        /// <summary>
        /// 创建自己的节点
        /// </summary>
        public static void CreateMonoHelperRoot()
        {
            GameObject obj = new GameObject("MonoHelperRoot");
            obj.transform.parent = JyApp.GameApp.transform;
            _ins = obj.AddComponent<MonoHelper>();
        }
    }
}
