﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRotation : MonoBehaviour
{
    private Transform tran;

    void Start()
    {
        var obj = new GameObject();
        obj.transform.parent = transform;
        tran = obj.transform;
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            tran.localEulerAngles = Vector3.zero;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            tran.Rotate(Vector3.zero);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            tran.localRotation = Quaternion.Euler(Vector3.zero);
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            tran.localEulerAngles = Vector3.up * 90;
        }
    }
}
